
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Acb extends HttpServlet {

    private ModeloDatos bd;

    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true); //crear sesion
        String nombreP = (String) req.getParameter("txtNombre");
        String nombre = (String) req.getParameter("R1");
        String btnVotar = (String) req.getParameter("B1");
        //String btnReset = (String) req.getParameter("B2");
        String btnVerVotos = (String) req.getParameter("B3");
        String btnVotosACero = (String) req.getParameter("B4");

        if(btnVotar!=null)
        {
            if (nombre.equals("Otros")) {
                nombre = (String) req.getParameter("txtOtros");
            }
            if (bd.existeJugador(nombre)) {
                bd.actualizarJugador(nombre);
            } else {
                bd.insertarJugador(nombre);
            }
            s.setAttribute("nombreCliente", nombreP);
            // Llamada a la página jsp que nos da las gracias
            res.sendRedirect(res.encodeRedirectURL("TablaVotos.jsp"));
        }
        if(btnVerVotos!=null)
        {
            String aviso="";
            s.setAttribute("avisoVotos",aviso);
            // Llamada a la página jsp de ver votos
            res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
        }
        if(btnVotosACero!=null)
        {
            bd.borraVotos();
            String aviso="Todos los votos han sido actualizados a cero";
            s.setAttribute("avisoVotos",aviso);
            //alert("Votacion reiniciada, votos a cero");
            res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
        }
    }

    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
